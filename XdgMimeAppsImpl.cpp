/**
 * IMPL Class
 *  This class will parse the mimeapps.list files.
 *  This is purely an implementational detail
 *  This class can be modified/removed at anytime
 */

#include "XdgMimeAppsImpl.hpp"

MimeAppsListParser::MimeAppsListParser( QString filename ) {
    mFileName = filename;
    mInfo     = QFileInfo( filename );

    fsw = new QFileSystemWatcher();
    connect(
        fsw, &QFileSystemWatcher::fileChanged, [ = ]( QString fn ) {
            /* File deleted and created again: add it to the watcher */
            if ( not fsw->files().contains( fn ) and QFile::exists( fn ) ) {
                fsw->addPath( fn );
            }

            /** Parse the file */
            parse();
        }
    );

    fsw->addPath( filename );

    parse();
}


MimeAppsListParser::~MimeAppsListParser() {
    /** Don't write unless there are pending changes */
    if ( modified ) {
        sync();
    }
}


/** Returns QVariant() if the section or key does not exist */
QVariant MimeAppsListParser::value( QString section, QString key ) {
    return sectKeyValueMap.value( section ).value( key );
}


/** Set the value in memort and immediately attempt to write it
 *  We need to write the whole file - there are no shortcuts here.
 *  Write it to a temporary file, then delete the original and
 *  rename the temporary file.
 */
void MimeAppsListParser::setValue( QString section, QString key, QVariant value ) {
    if ( not sectKeyValueMap.keys().contains( section ) ) {
        sectKeyValueMap[ section ] = QHash<QString, QVariant>();
    }

    /** Add the current key, value pair to the hash */
    sectKeyValueMap[ section ][ key ] = value;

    /** Set as modified */
    modified = true;

    /** Try to save the file */
    sync();
}


QStringList MimeAppsListParser::sections() {
    QStringList sectionList = sectKeyValueMap.keys();

    sectionList.sort( Qt::CaseInsensitive );

    return sectionList;
}


QStringList MimeAppsListParser::keys( QString section ) {
    QStringList keyList = sectKeyValueMap.value( section ).keys();

    keyList.sort( Qt::CaseInsensitive );

    return keyList;
}


void MimeAppsListParser::sync() {
    if ( not mInfo.isWritable() ) {
        return;
    }

    QString tmpFile = mFileName + QDateTime::currentDateTime().toString( "-yyyyMMddThhmmss" );
    QFile   f( tmpFile );

    if ( not f.open( QFile::WriteOnly ) ) {
        return;
    }

    QStringList sectionList = sections();

    for ( QString section: sectionList ) {
        /** Write the section name */
        f.write( ("[" + section + "]\n").toUtf8() );

        QStringList keyList = keys( section );
        for ( QString key: keyList ) {
            QVariant value = sectKeyValueMap[ section ][ key ];

            /** Write the key and the "=" */
            f.write( (key + "=").toUtf8() );

            /** Write the value: convert list to ';' separated string */
            if ( (QMetaType::Type)value.type() == QMetaType::QStringList ) {
                f.write( (value.toStringList().join( ";" ) + "\n").toUtf8() );
            }

            /** Write the value: string */
            else {
                f.write( (value.toString() + "\n").toUtf8() );
            }
        }

        /** Leave a gap between the sections */
        f.write( "\n" );
    }

    /** Writing to the temporary file complete. Copy to actual file */
    bool success = true;

    success &= QFile::remove( mFileName );          // We will simply assume that this worked.
    success &= QFile::rename( tmpFile, mFileName ); // We will simply assume that this worked.

    modified = not success;
}


/** Auto-recovery from crashes
 *  If the actual file is missing and /path/mimeapps.list-<yyyyMMddThhmmss> exists,
 *  We can assume that there was a crash before the rename operation could complete.
 *  So we can rename the temporary file to actual file can continue from there.
 */
void MimeAppsListParser::parse() {
    QFileInfo fileInfo( mFileName );

    /** Try to recover from an assumed crash */
    if ( not mInfo.exists() ) {
        /** Crash recovery possible only for writable locations */
        if ( not mInfo.isWritable() ) {
            return;
        }

        QString bestFit;

        QDateTime    latest;
        QDirIterator it( mInfo.filePath(), { "mimeapps.list-*T*" }, QDir::Files );
        while ( it.hasNext() ) {
            QString file = it.next();

            if ( file.startsWith( mFileName ) ) {
                QString   strDt = QString( file ).replace( mFileName + "-", "" );
                QDateTime newDt = QDateTime::fromString( strDt, "yyyyMMddThhmmss" );

                if ( newDt.isValid() and (newDt > latest) ) {
                    latest  = newDt;
                    bestFit = file;
                }
            }
        }

        /** If we found a file of bestFit */
        if ( not bestFit.isEmpty() ) {
            QFile::rename( bestFit, mFileName );
        }

        /** This file does not exist: Perhaps a fresh install or a new user? */
        else {
            QFile f( mFileName );
            f.open( QFile::WriteOnly );
            f.close();
        }

        /** Hopefully everything went well and we can add this file to the watch */
        fsw->addPath( mFileName );
    }

    QString lastSection;
    QFile   f( mFileName );

    if ( f.open( QFile::ReadOnly ) ) {
        while ( not f.atEnd() ) {
            /** We will be parsing the file, one line at a time */
            QString line( f.readLine() );

            /** Remove all the whitespaces at the beginning and the end */
            line = line.trimmed();

            /** The line starts with # - A comment */
            if ( line.startsWith( "#" ) ) {
                continue;
            }

            /** The line starts with [ - Probably a section. Ignore otherwise */
            else if ( line.startsWith( "[" ) ) {
                /** Okay, A section */
                if ( line.endsWith( "]" ) ) {
                    line.remove( 0, 1 );        // Remove the first character: [
                    line.chop( 1 );             // Remove the last character:  ]

                    lastSection = line;
                }

                /** Badly formatted line? */
                else {
                    continue;
                }
            }

            /** See if we have a key=value structure. Ignore otherwise */
            else {
                /** If we've not yet got a section, ignore the line */
                if ( lastSection.isEmpty() ) {
                    continue;
                }

                QStringList keyVal = line.split( "=", Qt::KeepEmptyParts );

                /** An entry can have only ine '=' */
                if ( keyVal.count() != 2 ) {
                    continue;
                }

                QString key( keyVal[ 0 ] );
                QString val( keyVal[ 1 ] );

                /** Ignore the line if either key or val is empty */
                if ( key.isEmpty() or val.isEmpty() ) {
                    continue;
                }

                QStringList values;

                if ( val.contains( ";" ) ) {
                    values = val.split( ";", Qt::SkipEmptyParts );
                }

                /** If @lastSection is not yet added to the hash, add it */
                if ( not sectKeyValueMap.keys().contains( lastSection ) ) {
                    sectKeyValueMap[ lastSection ] = QHash<QString, QVariant>();
                }

                /** Add the current key, value pair to the hash */
                if ( values.count() ) {
                    sectKeyValueMap[ lastSection ][ key ] = QVariant( values );
                }

                else {
                    sectKeyValueMap[ lastSection ][ key ] = QVariant( val );
                }
            }
        }
        f.close();
    }
}
