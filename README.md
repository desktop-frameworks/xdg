# DFL::XDG

The DFL::XDG namespace contains several functions that return paths as per the XDG Specifications - XDG_CONFIG_DIRS
DFL::XDG::DesktopFile class is a desktop file parser. DFL::XDG::ApplicationManager is the Mime-App manager.


### Dependencies:
* <tt>Qt5 Core and Gui (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/xdg.git dfl-xdg`
- Enter the `dfl-xdg` folder
  * `cd dfl-xdg`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any feature that you ask for.
