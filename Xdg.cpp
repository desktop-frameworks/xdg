/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *
 * The DFL::XDG namespace contains several functions that return
 * paths as per the XDG Specifications - XDG_CONFIG_DIRS
 * DFL::XDG::DesktopFile class is a desktop file parser.
 * DFL::XDG::ApplicationManager is the Mime-App manager.
 **/

#include <unistd.h>
#include <QStorageInfo>

#include "DFXdg.hpp"

#include "XdgDesktopImpl.hpp"
#include "XdgMimeAppsImpl.hpp"

/**
 * DFL::XDG Helper Functions
 *  These helper functions will return XDG standard locations
 */
QString DFL::XDG::homeDir() {
    QString home( qgetenv( "HOME" ) );

    if ( home.length() and QFile::exists( home ) ) {
        return home + (home.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath() + "/";
}


QString DFL::XDG::xdgCacheHome() {
    QString dir( qgetenv( "XDG_CACHE_HOME" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::home().filePath( ".cache/" );
}


QString DFL::XDG::xdgConfigHome() {
    QString dir( qgetenv( "XDG_CONFIG_HOME" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::home().filePath( ".config/" );
}


QString DFL::XDG::xdgDataHome() {
    QString dir( qgetenv( "XDG_DATA_HOME" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::home().filePath( ".local/share/" );
}


QString DFL::XDG::xdgStateHome() {
    QString dir( qgetenv( "XDG_STATE_HOME" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::home().filePath( ".local/state/" );
}


QStringList DFL::XDG::xdgConfigDirs() {
    QString dirs = qgetenv( "XDG_CONFIG_DIRS" );

    if ( dirs.length() ) {
        return dirs.split( ":", Qt::SkipEmptyParts );
    }

    return QStringList( { "/etc/xdg/" } );
}


QStringList DFL::XDG::xdgDataDirs() {
    QString dirs = qgetenv( "XDG_DATA_DIRS" );

    if ( dirs.length() ) {
        return dirs.split( ":", Qt::SkipEmptyParts );
    }

    return QStringList( { "/usr/local/share/", "/usr/share/" } );
}


QString DFL::XDG::xdgDesktopDir() {
    QString dir( qgetenv( "XDG_DESKTOP_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgDocumentsDir() {
    QString dir( qgetenv( "XDG_DOCUMENTS_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgDownloadDir() {
    QString dir( qgetenv( "XDG_DOWNLOAD_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgMusicDir() {
    QString dir( qgetenv( "XDG_MUSIC_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgPicturesDir() {
    QString dir( qgetenv( "XDG_PICTURES_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgPublicshareDir() {
    QString dir( qgetenv( "XDG_PUBLICSHARE_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgTemplatesDir() {
    QString dir( qgetenv( "XDG_TEMPLATES_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgVideosDir() {
    QString dir( qgetenv( "XDG_VIDEOS_DIR" ) );

    if ( dir.length() and QFile::exists( dir ) ) {
        return dir + (dir.endsWith( "/" ) ? "" : "/");
    }

    return QDir::homePath();
}


QString DFL::XDG::xdgRuntimeDir() {
    /* Get it from the env variable */
    QString dir( qgetenv( "XDG_RUNTIME_DIR" ) );

    /* Variable not set */
    if ( not dir.length() ) {
        dir = QString( "/run/user/%1" ).arg( getuid() );

        /* We will try to create /run/$UID */
        if ( QDir( "/" ).mkpath( dir ) ) {
            QFile::setPermissions( dir, QFile::ReadUser | QFile::WriteUser | QFile::ExeUser );
            return dir + (dir.endsWith( "/" ) ? "" : "/");
        }
    }

    /* So the variable is set, but does not exist */
    else if ( not QFile::exists( dir ) ) {
        /* Created successfully */
        if ( QDir( "/" ).mkpath( dir ) ) {
            QFile::setPermissions( dir, QFile::ReadUser | QFile::WriteUser | QFile::ExeUser );
            return dir + (dir.endsWith( "/" ) ? "" : "/");
        }
    }

    /** We failed with /run/user/$USER. Time for /tmp/$USER */
    dir = QString( "/tmp/%1/" ).arg( getuid() );

    if ( QDir( "/" ).mkpath( dir ) ) {
        QFile::setPermissions( dir, QFile::ReadUser | QFile::WriteUser | QFile::ExeUser );
        return dir;
    }

    return QString( "/tmp/" );
}


/**
 * DFL::XDG::sktopFile
 *  This class will parse, and optionally run, an app given its desktop file.
 *  Add/Remove the desktop urls from memory when an app is installed/uninstalled.
 *  It will also list the URLs for a given mimetype
 *  Provide the default app desktop url for a given mimetype.
 */

DFL::XDG::DesktopFile::DesktopFile( QString filename ) {
    if ( filename.isEmpty() ) {
        return;
    }

    QFileInfo info( filename );

    mFileUrl     = desktopPathForName( filename );
    mDesktopName = info.fileName();

    if ( mDesktopName.endsWith( ".desktop" ) ) {
        mDesktopName.chop( 8 );
    }

    if ( mFileUrl.startsWith( "/usr/share/applications" ) ) {
        mRank = 1;
    }

    else if ( mFileUrl.startsWith( "/usr/share/games" ) ) {
        mRank = 1;
    }

    else if ( mFileUrl.startsWith( "/usr/share/applications/kde4" ) ) {
        mRank = 2;
    }

    else if ( mFileUrl.startsWith( "/usr/share/gnome/applications" ) ) {
        mRank = 3;
    }

    else if ( mFileUrl.startsWith( "/usr/local/share/applications" ) ) {
        mRank = 4;
    }

    else if ( mFileUrl.startsWith( "/usr/local/share/games" ) ) {
        mRank = 4;
    }

    else if ( mFileUrl.startsWith( QDir::home().filePath( ".local/share/applications" ) ) ) {
        mRank = 5;
    }

    else if ( mFileUrl.startsWith( QDir::home().filePath( ".local/share/games" ) ) ) {
        mRank = 5;
    }

    else if ( mFileUrl.startsWith( QDir::home().filePath( ".local/share/applications/wine" ) ) ) {
        mRank = 5;
    }

    QMap<QString, QVariant> desktopMap = parseDesktop( mFileUrl );

    if ( desktopMap.empty() ) {
        return;
    }

    mName        = desktopMap.value( "Name" ).toString();
    mGenericName = desktopMap.value( "GenericName" ).toString();
    mDescription = desktopMap.value( "Comment" ).toString();
    mCommand     = desktopMap.value( "Exec" ).toString();
    mExec        = desktopMap.value( "TryExec", mCommand.split( " ", Qt::SkipEmptyParts ).value( 0 ) ).toString();

    if ( not QFile::exists( mExec ) ) {
        for ( QString path: QString::fromLocal8Bit( qgetenv( "PATH" ) ).split( ":", Qt::SkipEmptyParts ) ) {
            if ( QFile::exists( path + "/" + mExec ) ) {
                mExec = path + "/" + mExec;
                break;
            }
        }
    }

    mIcon = desktopMap.value( "Icon" ).toString();

    QStringList args = mCommand.split( " " );

    for ( QString arg: args ) {
        if ( arg == "%f" or arg == "%u" ) {
            mMultiArgs = false;
            mTakesArgs = true;
        }

        else if ( arg == "%F" or arg == "%U" ) {
            mMultiArgs = true;
            mTakesArgs = true;
        }
    }

    mMimeTypes  = desktopMap.value( "MimeType" ).toStringList();
    mCategories = desktopMap.value( "Categories" ).toStringList();

    mOnlyShowIn = desktopMap.value( "OnlyShowIn" ).toStringList();
    mNotShowIn  = desktopMap.value( "NotShowIn" ).toStringList();

    getCategory();

    mVisible = not desktopMap.value( "NoDisplay", false ).toBool();

    /** We need to check if we can show in this DE */
    for ( QString DE: qEnvironmentVariable( "XDG_CURRENT_DESKTOP" ).split( ";", Qt::SkipEmptyParts ) ) {
        /** Good if @DE is in mOnlyShowIn */
        if ( mOnlyShowIn.length() ) {
            mVisible &= mOnlyShowIn.contains( DE );
        }

        /** Good if @DE is NOT in mNotShowIn */
        mVisible &= not mNotShowIn.contains( DE );
    }

    mRunInTerminal = desktopMap.value( "Terminal", false ).toBool();

    if ( desktopMap.value( "Type" ).toString() == "Application" ) {
        mType = Application;
    }

    else if ( desktopMap.value( "Type" ).toString() == "Link" ) {
        mType = Link;
    }

    else if ( desktopMap.value( "Type" ).toString() == "Directory" ) {
        mType = Directory;
    }

    /** Update actions */
    mActionNames = desktopMap.value( "Actions" ).toStringList();
    for ( QString act: mActionNames ) {
        mActions[ act ] = desktopMap[ act ].toStringList();
    }

    if ( mName.length() and mCommand.length() ) {
        if ( not QFile::exists( mExec ) ) {
            mValid = false;
        }

        else {
            mValid = true;
        }
    }
}


bool DFL::XDG::DesktopFile::startApplication( QStringList args, QString action, QProcessEnvironment env ) {
    if ( not mValid ) {
        return false;
    }

    QStringList execList;

    /** Parse the exec */
    QStringList execArgs;

    if ( action.length() and mActionNames.contains( action ) ) {
        execArgs = mActions[ action ][ 2 ].split( " " );
    }

    else {
        execArgs = mCommand.split( " " );
    }

    QStringList parsedArgs;

    for ( QString arg: execArgs ) {
        if ( arg == "%f" or arg == "%u" ) {
            parsedArgs << "<#DESQARG-FILE#>";
        }

        else if ( arg == "%F" or arg == "%U" ) {
            parsedArgs << "<#DESQARG-FILES#>";
        }

        else if ( arg == "%i" ) {
            if ( !mIcon.isEmpty() ) {
                parsedArgs << "--icon" << mIcon;
            }
        }

        else if ( arg == "%c" ) {
            parsedArgs << mName;
        }

        else if ( arg == "%k" ) {
            parsedArgs << QUrl( mFileUrl ).toLocalFile();
        }

        else {
            parsedArgs << arg;
        }
    }

    /* If this is a commandline app */
    if ( mRunInTerminal ) {
        execList << QFileInfo( "/etc/alternatives/x-terminal-emulator" ).symLinkTarget() << "-e";
    }

    execList << parsedArgs;
    QString exec = execList.takeFirst();

    QStringList argList;

    /** No arguments provided */
    if ( args.length() == 0 ) {
        argList << execList;
        argList.removeAll( "<#DESQARG-FILES#>" );
        argList.removeAll( "<#DESQARG-FILE#>" );
    }

    /** Arguments provided. */
    else {
        if ( mTakesArgs ) {
            if ( mMultiArgs ) {
                for ( QString exeArg: execList ) {
                    if ( exeArg == "<#DESQARG-FILES#>" ) {
                        if ( args.length() ) {
                            argList << args;
                        }
                    }

                    else {
                        argList << exeArg;
                    }
                }
            }

            else {
                int idx = exec.indexOf( "<#DESQARG-FILE#>" );
                argList << execList;
                argList.removeAt( idx );

                if ( args.length() ) {
                    argList.insert( idx, args.takeAt( 0 ) );
                    argList << args;
                }
            }
        }

        else {
            argList << execList;

            if ( args.length() ) {
                argList << args;
            }
        }
    }

    QProcess proc;
    proc.setProgram( exec );
    proc.setArguments( argList );
    proc.setWorkingDirectory( env.contains( "PWD" ) ? env.value( "PWD" ) : QDir::homePath() );
    proc.setProcessEnvironment( env.isEmpty() ? QProcessEnvironment::systemEnvironment() : env );

    return proc.startDetached();
}


QString DFL::XDG::DesktopFile::desktopName() const {
    return mDesktopName;
}


QString DFL::XDG::DesktopFile::name() const {
    return mName;
}


QString DFL::XDG::DesktopFile::genericName() const {
    return mGenericName;
}


QString DFL::XDG::DesktopFile::description() const {
    return mDescription;
}


QString DFL::XDG::DesktopFile::executable() const {
    return mExec;
}


QString DFL::XDG::DesktopFile::command() const {
    return mCommand;
}


QString DFL::XDG::DesktopFile::icon() const {
    return mIcon;
}


QString DFL::XDG::DesktopFile::category() const {
    return mCategory;
}


QStringList DFL::XDG::DesktopFile::mimeTypes() const {
    return mMimeTypes;
}


QStringList DFL::XDG::DesktopFile::categories() const {
    return mCategories;
}


QStringList DFL::XDG::DesktopFile::actions() const {
    return mActionNames;
}


QStringList DFL::XDG::DesktopFile::action( QString id ) const {
    return mActions[ id ];
}


int DFL::XDG::DesktopFile::type() const {
    return mType;
}


int DFL::XDG::DesktopFile::rank() const {
    return mRank;
}


bool DFL::XDG::DesktopFile::visible() const {
    return mVisible;
}


bool DFL::XDG::DesktopFile::runInTerminal() const {
    return mRunInTerminal;
}


bool DFL::XDG::DesktopFile::multipleArgs() const {
    return mMultiArgs;
}


bool DFL::XDG::DesktopFile::isValid() const {
    return mValid;
}


QString DFL::XDG::DesktopFile::desktopFileUrl() const {
    return mFileUrl;
}


bool DFL::XDG::DesktopFile::operator==( const DFL::XDG::DesktopFile& other ) const {
    bool truth = true;

    truth &= (mCommand == other.command() );
    truth &= (mRank == other.rank() );

    return truth;
}


QString DFL::XDG::DesktopFile::desktopPathForName( QString desktopName ) {
    if ( not desktopName.endsWith( ".desktop" ) ) {
        desktopName += ".desktop";
    }

    if ( QFile::exists( desktopName ) ) {
        return desktopName;
    }

    /** Assuming desktopName is the name of the desktop */
    for ( QString appDirStr: DFL::XDG::ApplicationManager::applicationPaths() ) {
        if ( QFile::exists( appDirStr + "/" + desktopName ) ) {
            return appDirStr + "/" + desktopName;
        }
    }

    return QString();
}


void DFL::XDG::DesktopFile::getCategory() {
    QStringList Accessories = QStringList() << "Utility" << "Utilities" << "Accessory" << "Accessories";
    QStringList Development = QStringList() << "Development";
    QStringList Education   = QStringList() << "Education";
    QStringList Games       = QStringList() << "Games" << "Game" << "ArcadeGame" << "StrategyGame" << "LogicGame";
    QStringList Graphics    = QStringList() << "Graphics";
    QStringList Internet    = QStringList() << "Network" << "Internet";
    QStringList Multimedia  = QStringList() << "Audio" << "Video" << "AudioVideo" << "Multimedia";
    QStringList Office      = QStringList() << "Office";
    QStringList ScienceMath = QStringList() << "Science" << "Math";
    QStringList Settings    = QStringList() << "Settings";
    QStringList System      = QStringList() << "System";
    QStringList Wine        = QStringList() << "Wine";

    /* If the file name and the command contains 'wine', then it's a wine application */
    if ( mFileUrl.contains( "wine", Qt::CaseInsensitive ) and mCommand.contains( "wine", Qt::CaseInsensitive ) ) {
        mCategories = QStringList( { "Wine" } );
        mCategory   = "Wine";
        return;
    }

    else {
        mCategory = "Uncategorized";
        foreach( QString cate, mCategories ) {
            if ( Accessories.contains( cate ) ) {
                mCategory = "Accessories";
            }

            else if ( Development.contains( cate ) ) {
                mCategory = "Development";
            }

            else if ( ScienceMath.contains( cate ) ) {
                mCategory = "Science & Math";
            }

            else if ( Education.contains( cate ) ) {
                mCategory = "Education";
            }

            else if ( Games.contains( cate ) ) {
                mCategory = "Games";
            }

            else if ( Office.contains( cate ) ) {
                mCategory = "Office";
            }

            else if ( Graphics.contains( cate ) ) {
                mCategory = "Graphics";
            }

            else if ( Internet.contains( cate ) ) {
                mCategory = "Internet";
            }

            else if ( Multimedia.contains( cate ) ) {
                mCategory = "Multimedia";
            }

            else if ( Settings.contains( cate ) ) {
                mCategory = "Settings";
            }

            else if ( System.contains( cate ) ) {
                mCategory = "System";
            }

            else if ( Wine.contains( cate ) ) {
                mCategory = "Wine";
            }

            else {
                continue;
            }
        }
    }
}


bool DFL::XDG::DesktopFile::exists( QString app ) {
    QString desktopName( app );

    if ( not app.endsWith( ".desktop" ) ) {
        desktopName += ".desktop";
    }

    /** Full path is given */
    if ( desktopName.startsWith( "/" ) ) {
        QFileInfo desktopInfo( desktopName );
        desktopName = desktopInfo.baseName();
    }

    for ( QString path: DFL::XDG::ApplicationManager::applicationPaths() ) {
        if ( QFile::exists( path + "/" + desktopName ) ) {
            return true;
        }
    }

    return false;
}


/**
 * DFL::XDG::ApplicationManager
 *  This class will manage the desktops.
 *  Add/Remove the desktop urls from memory when an app is installed/uninstalled.
 *  It will also list the URLs for a given mimetype
 *  Provide the default app desktop url for a given mimetype.
 */

DFL::XDG::ApplicationManager::ApplicationManager() : QObject() {
    defaultWriter = new MimeAppsListParser( QDir::home().filePath( ".config/mimeapps.list" ) );
}


DFL::AppsList DFL::XDG::ApplicationManager::allApplications() {
    if ( not mParsingComplete ) {
        qWarning() << "The desktop files have not been parsed. Call parseDesktops() first.";
    }

    return mAllDesktops.values();
}


QStringList DFL::XDG::ApplicationManager::appsForMimeType( QString mimeType ) {
    if ( not mParsingComplete ) {
        qWarning() << "The desktop files have not been parsed. Call parseDesktops() first.";
    }

    return mimeAppsHash.value( mimeType );
}


QString DFL::XDG::ApplicationManager::defaultAppForMimeType( QString mimeType ) {
    if ( mParsingComplete == false ) {
        qWarning() << "The desktop files have not been parsed. Call parseDesktops() first.";
    }

    QString defApp( mimeDefaultAppHash.value( mimeType ) );

    if ( defApp.isEmpty() ) {
        QStringList others( mimeAppsHash.value( mimeType ) );

        if ( others.length() >= 1 ) {
            defApp = others[ 0 ];
        }
    }

    return defApp;
}


void DFL::XDG::ApplicationManager::setDefaultAppForMimeType( QString mimeType, QString desktop ) {
    defaultWriter->setValue( "Default Applications", mimeType, desktop );

    mimeDefaultAppHash[ mimeType ] = desktop;
}


QStringList DFL::XDG::ApplicationManager::applicationPaths() {
    return {
        QDir::home().filePath( ".local/share/applications/" ),
        QDir::home().filePath( ".local/share/applications/wine/" ),
        QDir::home().filePath( ".local/share/games/" ),
        "/usr/local/share/applications/",
        "/usr/local/share/games/",
        "/usr/share/applications/kde4/",            // Deprecated
        "/usr/share/gnome/applications/",           // Deprecated
        "/usr/share/applications/",
        "/usr/share/games/"
    };
}


void DFL::XDG::ApplicationManager::parseDesktops() {
    QStringList paths( applicationPaths() );

    std::reverse( paths.begin(), paths.end() );

    for ( QString appDir: paths ) {
        QDirIterator it( appDir, { "*.desktop" }, QDir::Files );

        while ( it.hasNext() ) {
            DFL::XDG::DesktopFile desktop( it.next() );

            if ( not desktop.isValid() ) {
                continue;
            }

            /** If it's not an application type desktop, ignore it */
            if ( desktop.type() != DFL::XDG::DesktopFile::Type::Application ) {
                continue;
            }

            /**
             * If we have a higher range coming later on,
             * the current value will be replaced by the newer one.
             */
            mAllDesktops[ desktop.desktopName() ] = desktop;

            QStringList mimeTypes( desktop.mimeTypes() );
            QString     desktopName( desktop.desktopName() );

            /**
             * First, if any mimetype in mimeAppsHash lists this desktop,
             * but, the current desktop (higher rank) does not contain that
             * mimetype, then remove the association.
             */
            for ( QString mimeType: mimeAppsHash.keys() ) {
                QStringList desktops( mimeAppsHash[ mimeType ] );

                if ( desktops.contains( desktopName ) and not mimeType.contains( mimeType ) ) {
                    desktops.removeAll( desktopName );
                    mimeAppsHash[ mimeType ] = desktops;
                }
            }

            /** Update the has with mimetypes of current desktop */
            for ( QString mimeType: desktop.mimeTypes() ) {
                if ( not mimeAppsHash.contains( mimeType ) ) {
                    mimeAppsHash[ mimeType ] = QStringList();
                }

                /** Add to the list only if it does not exist */
                if ( not mimeAppsHash.value( mimeType ).contains( desktopName ) ) {
                    mimeAppsHash[ mimeType ] << desktopName;
                }
            }

            /** Handle Added/Removed Associations of this path's mimeapps.list */
            MimeAppsListParser parser( appDir + "/mimeapps.list" );

            /** Added Associations */
            for ( QString key: parser.keys( "Added Associations" ) ) {
                QVariant value = parser.value( "Added Associations", key );

                if ( not mimeAppsHash.contains( key ) ) {
                    mimeAppsHash[ key ] = QStringList();
                }

                QStringList desktops( mimeAppsHash[ key ] );
                for ( QString desktopName: value.toStringList() ) {
                    if ( DFL::XDG::DesktopFile( desktopName ).isValid() and not desktops.contains( desktopName ) ) {
                        mimeAppsHash[ key ] << desktopName;
                    }
                }
            }

            /** Removed Associations */
            for ( QString key: parser.keys( "Removed Associations" ) ) {
                QVariant value = parser.value( "Removed Associations", key );

                QStringList desktops( mimeAppsHash.value( key ) );
                for ( QString desktopName: value.toStringList() ) {
                    desktops.removeAll( desktopName );
                    mimeAppsHash[ key ] = desktops;
                }
            }
        }
    }

    /** Parsing mimeapps.list
     *  1. Parse /usr/share/mimeapps.list
     *  2. Parse /usr/local/share/mimeapps.list (Will override /usr/share/)
     *  3. Parse ~/.local/share/mimeapps.list (Will override /usr/share/ and /usr/local/share)
     *
     *  First valid desktop will be the default
     */

    QStringList appPaths = {
        "/usr/share/applications/",
        "/usr/share/applications/kde4/",
        "/usr/share/gnome/applications/",
        "/usr/local/share/applications/",
        QDir::home().filePath( ".config/" ),
    };

    for ( QString path: appPaths ) {
        MimeAppsListParser parser( path + "/mimeapps.list" );
        for ( QString key: parser.keys( "Default Applications" ) ) {
            QVariant value = parser.value( "Default Applications", key );

            for ( QString desktopName: value.toStringList() ) {
                if ( DFL::XDG::DesktopFile( desktopName ).isValid() ) {
                    mimeDefaultAppHash[ key ] = desktopName;
                    break;
                }
            }
        }
    }

    mParsingComplete = true;
}


uint qHash( const DFL::XDG::DesktopFile& app ) {
    QString hashString;

    hashString += app.name();
    hashString += app.genericName();
    hashString += app.description();
    hashString += app.command();
    hashString += app.icon();
    hashString += app.mimeTypes().join( " " );
    hashString += app.categories().join( " " );

    return qChecksum( hashString.toLocal8Bit().data(), hashString.length() );
}
