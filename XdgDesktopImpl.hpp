/**
 * IMPL Class
 *  This class will parse the desktop entry files.
 *  This is purely an implementational detail
 *  This file and/or functions/classes within can be modified/removed at anytime
 */

#pragma once

#include <QtCore>

QMap<QString, QVariant> parseDesktop( QString filename );
